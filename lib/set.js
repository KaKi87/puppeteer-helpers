module.exports = (object, path, value) => {
    let _object = object;
    for(let i = 0; i < path.length - 1; i++){
        const key = path[i];
        if(!_object[key])
            _object[key] = {};
        _object = _object[key];
    }
    _object[path[path.length - 1]] = value;
};