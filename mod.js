const
    createBrowserHelper = require('./src/browserHelper.js'),
    createPageHelper = require('./src/pageHelper.js'),
    createUserPreferences = require('./src/userPreferences.js');
module.exports = {
    createBrowserHelper,
    createPageHelper,
    createUserPreferences
};