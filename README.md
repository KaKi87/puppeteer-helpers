# `puppeteer-helpers`

[Documentation](./DOCUMENTATION.md)

## Basic usage (using `puppeteer`)

```js
const
    puppeteer = require('puppeteer'),
    {
        createBrowserHelper,
        createPageHelper,
    } = require('puppeteer-helpers');

(async () => {
    const
        myBrowser = await puppeteer.launch(),
        myBrowserHelper = createBrowserHelper(myBrowser),
        myPage = await myBrowserHelper.getDefaultPage(),
        myPageHelper = createPageHelper(myPage);
})();
```

## Advanced usage (using `puppeteer-extra`)

```js
const
    puppeteer = require('puppeteer-extra'),
    {
        createBrowserHelper,
        createPageHelper,
        createUserPreferences
    } = require('puppeteer-helpers');

const myUserPreferences = createUserPreferences();
myUserPreferences.set(['webkit', 'webprefs', 'default_font_size'], 22);
myUserPreferences.setDevToolsDocked(false);
myUserPreferences.setDevToolsTheme('dark');
puppeteer.use(require('puppeteer-extra-plugin-user-preferences')(myUserPreferences.get()));

(async () => {
    const
        myBrowser = await puppeteer.launch(),
        myBrowserHelper = createBrowserHelper(myBrowser),
        myPage = await myBrowserHelper.getDefaultPage(),
        myPageHelper = createPageHelper(myPage);
})();
```