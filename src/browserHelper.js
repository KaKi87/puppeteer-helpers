/** @param {import('@types/puppeteer').Browser} browser */
module.exports = browser => ({
    /**
     * Get the page initially created when starting Chromium.
     * @returns {Promise<import('@types/puppeteer').Page>}
     */
    getDefaultPage: async () => (await browser.pages())[0],
    /**
     * Create a private browsing page, isolated from other normal & private browsing pages.
     * @returns {Promise<import('@types/puppeteer').Page>}
     */
    createIncognitoPage: async () => await (await browser.createIncognitoBrowserContext()).newPage(),
    /**
     * Wait for a popup page to be created.
     * @returns {Promise<import('@types/puppeteer').Page>}
     */
    waitForPopup: () => new Promise(resolve => {
        const onTargetCreated = target => {
            if(target.type() === 'page'){
                browser.removeListener('targetcreated', onTargetCreated);
                resolve(target.page());
            }
        };
        browser.on('targetcreated', onTargetCreated);
    })
});