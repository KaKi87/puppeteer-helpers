/** @param {import('@types/puppeteer').Page} page */
module.exports = page => {
    /**
     * Get an element's property, equivalent to `document.querySelector(selector)[property]`.
     * @param {string} selector
     * @param {string} property
     * @returns {Promise<string>}
     */
    const getProperty = async (selector, property) => await (await (await page.waitForSelector(selector)).getProperty(property)).jsonValue();
    return {
        getProperty,
        /**
         * Replace an input's value.
         * @param {string} selector
         * @param {string} value
         * @returns {Promise}
         */
        clearAndType: async (selector, value) => {
            await page.click(selector, { clickCount: 3 });
            await page.keyboard.type(value);
        },
        /**
         * Wait for an element's `transitionend` event, uses `evaluate`.
         * @param {string} selector
         * @returns {Promise}
         */
        waitForTransitionEnd: async selector =>
            await (await page.waitForSelector(selector)).evaluate(
                element => new Promise(resolve => element.addEventListener('transitionend', resolve, { once: true })),
                selector
            ),
        /**
         * Select an option by index, equivalent to `document.querySelector(selector).selectedIndex = index`.
         * @param {string} selector
         * @param {number} index
         * @returns {Promise}
         */
        selectIndex: async (selector, index) => await page.select(selector, await getProperty(`${selector} option:nth-child(${index + 1})`, 'value')),
        /**
         * Get an element's computed style, uses `evaluate`.
         * @param {string} selector
         * @param  {string} property
         * @returns {Promise<string>}
         */
        getStyleProperty: async (selector, property) => (await page.waitForSelector(selector)).evaluate(
            (element, _, property) => window.getComputedStyle(element).getPropertyValue(property),
            selector, property
        ),
        /**
         * Get an input's value, uses `evaluate`.
         * @param {string} selector
         * @returns {Promise<string>}
         */
        getInputValue: async selector => (await page.waitForSelector(selector)).evaluate(element => element['value'], selector),
        /**
         * Display an alert dialog, uses `evaluate`.
         * @param {string} message
         * @returns {Promise}
         */
        alert: async message => await page.evaluate(message => window.alert(message), message),
        /**
         * Set the browser's file download path.
         * @param {string} downloadPath
         * @returns {Promise}
         */
        setDownloadPath: downloadPath => page['_client'].send('Page.setDownloadBehavior', { behavior: 'allow', downloadPath }),
        /**
         * Get an element's visibility state or existence.
         * @param {string} selector
         * @returns {Promise<boolean>}
         */
        isVisible: async selector => {
            try {
                await page.waitForSelector(selector, { visible: true, timeout: 1 });
                return true;
            }
            catch {
                return false;
            }
        },
        /**
         * Override the page title, change-resistant.
         * @param {string} title
         * @returns {void}
         */
        setTitle: title => {
            page.on('domcontentloaded', async () => {
                await page.evaluate(
                    title => {
                        document.title = title;
                        const titleElement = document.querySelector('title');
                        if(titleElement) new MutationObserver(() => {
                            if(document.title !== title)
                                document.title = title;
                        }).observe(titleElement, { childList: true })
                    },
                    title
                );
            });
        },
        /**
         * Create a [`MutationObserver`](https://developer.mozilla.org/docs/Web/API/MutationObserver). Uses `evaluate`.
         * @param {string} selector
         * @param {object} options
         * @param {function} callback
         * @param {string} callbackName
         * @returns {Promise}
         */
        createMutationObserver: async (
            selector,
            options,
            callback,
            callbackName = callback.name
        ) => {
            await page.exposeFunction(callbackName, callback);
            await page.evaluate(
                ({ callbackName, selector, options }) => new MutationObserver(window[callbackName]).observe(document.querySelector(selector), options),
                { callbackName, selector, options }
            );
        },
        /**
         * Click an element as long as possible.
         * @param {string} selector
         * @param {function(error: Error): boolean} [isErrorIgnored]
         * @returns {Promise}
         */
        forceClick: (selector, isErrorIgnored) => new Promise(resolve => {
            let element;
            (async function _(){
                try {
                    element = element ? await page.$(selector) : await page.waitForSelector(selector);
                    await element.click();
                    setTimeout(_, 0);
                }
                catch(error){
                    if(isErrorIgnored && isErrorIgnored(error))
                        setTimeout(_, 0);
                    else
                        resolve();
                }
            })();
        })
    };
};