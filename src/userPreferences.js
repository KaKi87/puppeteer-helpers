const _set = require('../lib/set.js');

module.exports = () => {
    /**
     * Set a preference.
     * @param {string[]} path
     * @param value
     */
    const
        userPreferences = {},
        set = (path, value) => _set(userPreferences, ['userPrefs', ...path], value);
    return {
        get: () => JSON.parse(JSON.stringify(userPreferences)),
        set,
        /**
         * Set default DevTools docking mode
         * @param {false|'left'|'bottom'|'right'} docking
         */
        setDevToolsDocked: (docking = 'right') => set(
            ['devtools', 'preferences', 'currentDockState'],
            `"${docking || 'undocked'}"`
        ),
        /**
         * Set default DevTools theme
         * @param {'light'|'dark'} theme
         */
        setDevToolsTheme: (theme = 'light') => set(
            ['devtools', 'preferences', 'uiTheme'],
            `"${theme}"`
        )
    };
};